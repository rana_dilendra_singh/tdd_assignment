import pytest
from pytest import raises
from unittest.mock import MagicMock
from code import AddTwoNumber

class TestCode:
    @classmethod
    def setup_class(cls):
        print("\n\nStarting setup_class\n")

    @classmethod
    def teardown_class(cls):
        print("\nteardown_class called\n\n")

    def test_add_two_number(self):
        assert 16 == AddTwoNumber.add_two_number(6,10)

    def test_add_two_number_str_one(self):
        assert '30' == AddTwoNumber.add_two_number('20', 10) #one string input

    def test_add_two_number_both_str(self):
        assert '30' == AddTwoNumber.add_two_number('20', '10') #both string input

    def test_add_two_number_one_none(self):
        assert AddTwoNumber.add_two_number(None,10) is None

    def test_add_two_number_none(self):
        assert AddTwoNumber.add_two_number(None,None) is None

    def test_return(self, AddTwoNumber, monkeypatch):
        mock_file = MagicMock()
        mock_file.readlines = MagicMock(a=2,b=8)
        mock_open = MagicMock(a=mock_file[0],b=mock_file[1])
        monkeypatch.setattr("builtins.open", mock_open)
        result = AddTwoNumber.read_from_file("input")
        mock_open.assert_called_once_with("input", "r")
        assert AddTwoNumber.add_two_number(result) == 10


