class AddTwoNumber:
    def add_two_number(num1 , num2):
        if (type(num1) == str or type(num2)==str):
            num11 = int(num1)
            num21 = int(num2)

            result_sum= num11 + num21
            return str(result_sum)
        elif (num1 is None) or num2 is None :
            return None
        else:
            return num1+num2

    def read_from_file(self, filename):
        infile = open(filename, "r")
        line = infile.readlines()
        return line[0],line[1]



